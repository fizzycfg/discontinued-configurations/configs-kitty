# Kitty Configuration

Kitty configuration (fizzy-compliant)

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-kitty/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
